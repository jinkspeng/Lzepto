var $ = (function(d) {
	var slice = [].slice,
		ADJ_OPS = {
			append: 'beforeEnd',
			before: 'beforeBegin',
			before: 'beforeBegin',
			after: 'afterEnd'
		}

	function $(_) {
		function fn(_) {
			return arguments.callee.dom.forEach(_), arguments.callee;  //简化书写
		};
		//初始化处理字符串和节点
		fn.dom = _ instanceof Element ? [_] : slice.call(d.querySelectorAll(fn.selector = _));
		for(k in $.fn) fn[k] = $.fn[k];
		return fn;
	}

	function classRE(name) {
		return new RegExp("(^|\\s)"+name+"(\\s|$)"); //判断className的正则
	}

	$.fn = {
		get: function(idx) {
			return idx === void 0 ? this.dom : this.dom[idx]; //使用void替代undefined
		},
		html: function(html) {
			return (html === void 0) ? this.dom[0].innerHTML :
			this(function(el) {
				el.innerHTML = html;
			})
		},
		attr: function(name, value) {
			return (value == void 0) ? this.dom[0].getAttribute(name) || void 0 : //三目运算符优先级高
			this(function(el) {
				el.setAttribute(name, value);
			})
		},
		css: function(style) {
			return this(function(el) {
				el.style.cssText += ';' + style;
			})
		},
		index: function(target) {
			return [].indexOf.call(this.dom, $(target).get(0));
		},
		anim: function(transform, opacity, dur) {
			return this.css('-webkit-transition:all '+(dur || 0.5) + 's;' +
				'-webkit-transform:' + transform + ';opacity:' + (opacity === 0 ? 0 : opacity || 1));
		},
		delegate: function(selector, event, callback) {
			this(function(el) {
				el.addEventListener(event, function(event) {
					var target = event.target, nodes = slice.call(el.querySelectorAll(selector));
					while(target && nodes.indexOf(target) < 0) target = target.parentNode;
					if(target && !(target === el) && !(target === d)) callback.call(target, event);
				}, false)
			});
		},
		addClass: function(name) {
			return this(function(el) {
				!classRE(name).test(el.className) && (el.className +=
					(el.className ? ' ' : '') + name); //是否添加空格的处理
			})
		},
		removeClass: function(name) {
			return this(function(el) {
				el.className = el.className.replace(classRE(name), ' ') //先用空格替代，再去除多余的空格
					.replace(/^\s+|\s+$/g, '');
			})
		}
	}

	for(k in ADJ_OPS) {
	   $.fn[k] = (function(op) {
		 return function(html) {
			return this(function(el) {
				el.insertAdjacentHTML(op,html)
			})
		 };
	   })(ADJ_OPS[k])
	}

	;(function () {
		function ajax (method, url, success) {
			var r  = new XMLHttpRequest();
			r.onreadystatechange = function() {
				if(r.readyState == 4 && (r.state == 200 || r.state == 0))
					success(r.responseText);
			}
			r.open(method, url, true);
			r.send(null);
		}
		$.get = function(url, success) {
			ajax('GET', url, success);
		}
		$.post = function(url, success) {
			ajax('POST', url, success);
		}
		$.getJSON = function(url, success) {
			$.get(url, function(json) {
				success(JSON.parse(json))
			});
		}
	})()
	return $;
})(document)
